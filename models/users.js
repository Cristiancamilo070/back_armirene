const User = require("./user");


class Users {

    constructor() {
        this.users = [];
    }

    addUser( user = new User() ) {
        this.users.push( user );
    }
    updateUser( user = new User() ,id='') {
        this.users = this.users.map( userx => {
            if ( user.id === id ) {
                this.users.push( user );
                return userx;
            } else {
                return userx;
            }

        });
    }

    getUsers() {
        return this.users;
    }

    deleteUser( id = '' ) {
        this.users = this.users.filter( user => user.id !== id );
        return this.users;
    }

    timesItWentUser( id = '' ) {

        this.users = this.users.map( user => {
            if ( user.id === id ) {
                user.votes++;
                return user;
            } else {
                return user;
            }

        });

    }

}


module.exports = Users;