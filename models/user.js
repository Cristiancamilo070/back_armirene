const { v4: uuidV4 } = require('uuid');

class User {

    constructor(firstName='no-first-name',otherNames='no-other-names',firstLastName='no-first-Last-Name',secondLastName='no-second-Last-Name',country='no-country', typeUserId='no-type-user-id',userId='no-user-id',entryDate='no-entry-date',jobArea='no-job-area',nowDate='no-now-date',email='no-email',votes=0) {
        
        this.id = uuidV4(); // identificador único
        this.firstName=firstName;
        this.otherNames=otherNames;
        this.firstLastName=firstLastName;
        this.secondLastName=secondLastName;
        this.country=country;
        this.typeUserId=typeUserId;
        this.userId=userId;
        this.email='';
        this.entryDate=entryDate;
        this.jobArea=jobArea;
        this.nowDate=nowDate;
        this.email=email;
        this.votes = votes;//borrar?
    }

}

module.exports = User;