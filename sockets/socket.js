const { io } = require('../index');

const Users = require('../models/users');
const User = require('../models/user');

const users = new Users();

users.addUser( new User( 'Cristian','Camilo','Cruz','Bustos','Colombia','CC','1015480161','22/02/2022','Desarrollo','22/02/2022','cristiancruz@armirene.com.co',5) );
users.addUser( new User( 'Juan','Nicolas','Vera','Gomez','Estados Unidos','CC','1234DF2454','22/02/2022','Desarrollo','22/02/2022','juanvera@armirene.com.co',2) );
users.addUser( new User( 'Paula','Andrea','Hernandez','Huertas','Colombia','CC','1234DF2412','22/02/2022','Desarrollo','22/02/2022','paulahernandez2@armirene.com.co',3) );
users.addUser( new User( 'Camila','Sofia','Espitia','Carrera','Estados Unidos','CC','1234DF2434','22/02/2022','Desarrollo','22/02/2022','sofiaespitia@armirene.com.co',5) );

users.addUser( new User( 'Cristian','Camilo','Cruz','Bustos','Colombia','CC','1015480162','22/02/2022','Desarrollo','22/02/2022','cristiancruz2@armirene.com.co',5) );
users.addUser( new User( 'Juan','Nicolas','Vera','Gomez','Estados Unidos','CC','1234DF2452','22/02/2022','Desarrollo','22/02/2022','juanvera2@armirene.com.co',2) );
users.addUser( new User( 'Paula','Andrea','Hernandez','Huertas','Colombia','CC','1234DF2414','22/02/2022','Desarrollo','22/02/2022','paulahernandez2@armirene.com.co',3) );
users.addUser( new User( 'Camila','Sofia','Espitia','Carrera','Estados Unidos','CC','1234DF245','22/02/2022','Desarrollo','22/02/2022','sofiaespiti2@armirene.com.co',5) );

// Mensajes de Sockets
io.on('connection', client => {
    console.log('Cliente conectado');

    client.emit('active-users', users.getUsers() );

    client.on('disconnect', () => {
        console.log('Cliente desconectado');
    });

    client.on('mensaje', ( payload ) => {
        console.log('Mensaje', payload);
        io.emit( 'mensaje', { admin: 'Nuevo mensaje' } );
    });

    client.on('times-it-went-user', (payload) => {

        users.timesItWentUser( payload.id );
        io.emit('active-users', users.getUsers() );
    });

    client.on('add-user', (payload) => {
        const newUser = new User( payload.firstName, payload.otherNames, payload.firstLastName, payload.secondLastName,payload.country, payload.typeUserId, payload.userId, payload.entryDate, payload.jobArea, payload.nowDate, payload.email);
        users.addUser( newUser );
        io.emit('active-users', users.getUsers() );
    });

    client.on('update-user', (payload) => {
        const updateUser =new User( payload.firstName, payload.otherNames, payload.firstLastName, payload.secondLastName,payload.country, payload.typeUserId, payload.userId, payload.entryDate, payload.jobArea, payload.nowDate, payload.email);
        users.updateUser( updateUser, payload.id );
        io.emit('active-users', payload.id);
    });

    client.on('delete-user', (payload) => {

        users.deleteUser( payload.id );
        io.emit('active-users', users.getUsers() );
    });

    // client.on('emitir-mensaje', ( payload ) => {
    //     // console.log(payload);
    //     // io.emit('nuevo-mensaje', payload ); // emite a todos!
    //     client.broadcast.emit('nuevo-mensaje', payload ); // emite a todos menos el que lo emitió
    // })

});
